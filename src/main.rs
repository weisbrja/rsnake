use std::time::Duration;

use bevy::prelude::*;

use rand::prelude::SliceRandom;
use rand::thread_rng;

const ARENA_WIDTH: u32 = 10;
const ARENA_HEIGHT: u32 = 10;

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            width: 600.,
            height: 600.,
            position: Some(Vec2::new(500., 200.)),
            title: "Snake".to_string(),
            resizable: false,
            ..Default::default()
        })
        .insert_resource(ClearColor(Color::hex("282828").unwrap()))
        .insert_resource(SnakeSegments::default())
        .insert_resource(LastTailPosition::default())
        .insert_resource(InputTimer::new(Duration::from_millis(200)))
        .add_event::<SnakeGrowthEvent>()
        .add_event::<SpawnFoodEvent>()
        .add_event::<GameOverEvent>()
        .add_plugins(DefaultPlugins)
        .add_startup_system(setup_camera.label("setup"))
        .add_startup_system(spawn_snake.label("spawn_snake").after("setup"))
        .add_startup_system(spawn_food.after("spawn_snake"))
        .add_system(snake_input.label(State::Input))
        .add_system(snake_movement.label(State::Movement).after(State::Input))
        .add_system(snake_eat.label(State::Eating).after(State::Movement))
        .add_system(snake_growth_event.label(State::Growth).after(State::Eating))
        .add_system(
            spawn_food_event
                .label(State::SpawnFood)
                .after(State::Growth),
        )
        .add_system(game_over_event.after(State::Movement))
        .add_system_set_to_stage(
            CoreStage::PostUpdate,
            SystemSet::new()
                .with_system(size_scaling)
                .with_system(position_translation)
                .with_system(sprites),
        )
        .run();
}

#[derive(SystemLabel, Debug, Hash, PartialEq, Eq, Clone)]
enum State {
    Input,
    Movement,
    Eating,
    Growth,
    SpawnFood,
}

#[derive(Component)]
struct SnakeHead {
    direction: Direction,
}

#[derive(Component)]
struct SnakeSegment;

#[derive(Clone, Copy)]
enum SnakeSpriteIndex {
    HeadUp,
    HeadLeft,
    HeadDown,
    HeadRight,
    TailUp,
    TailLeft,
    TailDown,
    TailRight,
    TopLeft,
    BottomLeft,
    BottomRight,
    TopRight,
    Vertical,
    Horizontal,
}

#[derive(Default)]
struct SnakeSegments(Vec<Entity>);

#[derive(Default)]
struct LastTailPosition(Option<Position>);

struct InputTimer {
    timer: Timer,
    received_input: bool,
}

impl InputTimer {
    fn new(duration: Duration) -> Self {
        Self {
            timer: Timer::new(duration, true),
            received_input: false,
        }
    }

    fn trigger(&mut self) {
        self.received_input = true;
        self.timer.reset();
    }

    fn tick_and_elapsed(&mut self, delta: Duration) -> bool {
        if self.received_input || self.timer.tick(delta).just_finished() {
            self.received_input = false;
            return true;
        }
        false
    }
}

#[derive(PartialEq, Eq, Copy, Clone)]
enum Direction {
    Up,
    Left,
    Down,
    Right,
}

impl Direction {
    fn opposite(self) -> Self {
        match self {
            Self::Up => Self::Down,
            Self::Left => Self::Right,
            Self::Down => Self::Up,
            Self::Right => Self::Left,
        }
    }
}

struct SnakeGrowthEvent;
struct SpawnFoodEvent;
struct GameOverEvent;

#[derive(Component)]
struct Food;

#[derive(Component, Clone, Copy, PartialEq, Eq)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn direction_to(&self, neighbor: Position) -> Direction {
        match (neighbor.x - self.x, neighbor.y - self.y) {
            (0, 1) => Direction::Up,
            (-1, 0) => Direction::Left,
            (0, -1) => Direction::Down,
            (1, 0) => Direction::Right,
            _ => unreachable!(),
        }
    }
}

#[derive(Component)]
struct PixelSize {
    width: u32,
    height: u32,
}

impl PixelSize {
    fn square(x: u32) -> Self {
        Self {
            width: x,
            height: x,
        }
    }
}

fn setup_camera(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}

fn spawn_snake(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut segments: ResMut<SnakeSegments>,
) {
    segments.0 = vec![
        commands
            .spawn_bundle(spawn_sprite_bundle(&asset_server, &mut texture_atlases))
            .insert(SnakeHead {
                direction: Direction::Up,
            })
            .insert(SnakeSegment)
            .insert(Position { x: 5, y: 5 })
            .insert(PixelSize::square(12))
            .id(),
        spawn_segment(
            commands,
            &asset_server,
            &mut texture_atlases,
            Position { x: 5, y: 4 },
        ),
    ];
}

fn spawn_sprite_bundle(
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
) -> SpriteSheetBundle {
    let texture_handle = asset_server.load("snake_parts.png");
    let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(12., 12.), 4, 4);
    let texture_atlas_handle = texture_atlases.add(texture_atlas);
    SpriteSheetBundle {
        texture_atlas: texture_atlas_handle,
        ..Default::default()
    }
}

fn spawn_segment(
    mut commands: Commands,
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
    pos: Position,
) -> Entity {
    commands
        .spawn_bundle(spawn_sprite_bundle(asset_server, texture_atlases))
        .insert(SnakeSegment)
        .insert(pos)
        .insert(PixelSize::square(12))
        .id()
}

fn snake_input(
    keyboard_input: Res<Input<KeyCode>>,
    mut timer: ResMut<InputTimer>,
    mut head: Query<&mut SnakeHead>,
) {
    let mut head = head.single_mut();
    let dir = if keyboard_input.just_pressed(KeyCode::H)
        || keyboard_input.just_pressed(KeyCode::Left)
    {
        Direction::Left
    } else if keyboard_input.just_pressed(KeyCode::J) || keyboard_input.just_pressed(KeyCode::Down)
    {
        Direction::Down
    } else if keyboard_input.just_pressed(KeyCode::K) || keyboard_input.just_pressed(KeyCode::Up) {
        Direction::Up
    } else if keyboard_input.just_pressed(KeyCode::L) || keyboard_input.just_pressed(KeyCode::Right)
    {
        Direction::Right
    } else {
        return;
    };

    if dir != head.direction.opposite() {
        head.direction = dir;
        timer.trigger();
    }
}

fn snake_movement(
    time: Res<Time>,
    mut timer: ResMut<InputTimer>,
    segment_entities: ResMut<SnakeSegments>,
    mut last_tail_pos: ResMut<LastTailPosition>,
    mut game_over_writer: EventWriter<GameOverEvent>,
    mut heads: Query<(Entity, &mut SnakeHead)>,
    mut positions: Query<&mut Position>,
) {
    let (head_entity, head) = heads.single_mut();
    if timer.tick_and_elapsed(time.delta()) {
        let old_segment_positions: Vec<_> = segment_entities
            .0
            .iter()
            .map(|e| *positions.get_mut(*e).unwrap())
            .collect();

        let mut head_pos = positions.get_mut(head_entity).unwrap();
        match head.direction {
            Direction::Left => head_pos.x -= 1,
            Direction::Down => head_pos.y -= 1,
            Direction::Up => head_pos.y += 1,
            Direction::Right => head_pos.x += 1,
        }

        if head_pos.x < 0
            || head_pos.y < 0
            || head_pos.x >= ARENA_WIDTH as i32
            || head_pos.y >= ARENA_HEIGHT as i32
            || old_segment_positions.contains(&head_pos)
        {
            game_over_writer.send(GameOverEvent);
        }

        for (entity, pos) in segment_entities
            .0
            .iter()
            .skip(1)
            .zip(old_segment_positions.iter())
        {
            *positions.get_mut(*entity).unwrap() = *pos;
        }

        last_tail_pos.0 = Some(*old_segment_positions.last().unwrap())
    }
}

fn size_scaling(windows: Res<Windows>, mut query: Query<(&PixelSize, &mut Transform)>) {
    let window = windows.get_primary().unwrap();
    for (size, mut transform) in query.iter_mut() {
        transform.scale = Vec3::new(
            window.width() / (ARENA_WIDTH * size.width) as f32,
            window.height() / (ARENA_HEIGHT * size.height) as f32,
            1.,
        );
    }
}

fn position_translation(windows: Res<Windows>, mut query: Query<(&Position, &mut Transform)>) {
    fn convert(pos: f32, bound_window: f32, bound_game: f32) -> f32 {
        let tile_size = bound_window / bound_game;
        pos / bound_game * bound_window - bound_window / 2. + tile_size / 2.
    }

    let window = windows.get_primary().unwrap();
    for (pos, mut transform) in query.iter_mut() {
        transform.translation = Vec3::new(
            convert(pos.x as f32, window.width(), ARENA_WIDTH as f32),
            convert(pos.y as f32, window.height(), ARENA_HEIGHT as f32),
            0.,
        );
    }
}

fn spawn_food(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    snake_positions: Query<&Position, With<SnakeSegment>>,
) {
    let snake_positions: Vec<_> = snake_positions.iter().collect();
    let free_positions: Vec<_> = (0..ARENA_WIDTH as i32)
        .flat_map(|x| (0..ARENA_HEIGHT as i32).map(move |y| Position { x, y }))
        .filter(|pos| !snake_positions.contains(&pos))
        .collect();
    let pos = *free_positions.choose(&mut thread_rng()).unwrap();

    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("apple.png"),
            ..Default::default()
        })
        .insert(Food)
        .insert(pos)
        .insert(PixelSize::square(12));
}

fn spawn_food_event(
    commands: Commands,
    asset_server: Res<AssetServer>,
    mut spawn_food_reader: EventReader<SpawnFoodEvent>,
    snake_positions: Query<&Position, With<SnakeSegment>>,
) {
    if spawn_food_reader.iter().next().is_some() {
        spawn_food(commands, asset_server, snake_positions);
    }
}

fn snake_eat(
    mut commands: Commands,
    mut growth_writer: EventWriter<SnakeGrowthEvent>,
    food_positions: Query<(Entity, &Position), With<Food>>,
    head_positions: Query<&Position, With<SnakeHead>>,
) {
    if let Some(head_pos) = head_positions.iter().next() {
        if let Some((entity, food_pos)) = food_positions.iter().next() {
            if food_pos == head_pos {
                commands.entity(entity).despawn();
                growth_writer.send(SnakeGrowthEvent);
            }
        }
    }
}

fn snake_growth_event(
    commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    last_tail_pos: Res<LastTailPosition>,
    mut segment_entities: ResMut<SnakeSegments>,
    mut growth_reader: EventReader<SnakeGrowthEvent>,
    mut spawn_food_writer: EventWriter<SpawnFoodEvent>,
) {
    if growth_reader.iter().next().is_some() {
        segment_entities.0.push(spawn_segment(
            commands,
            &asset_server,
            &mut texture_atlases,
            last_tail_pos.0.unwrap(),
        ));

        spawn_food_writer.send(SpawnFoodEvent);
    }
}

fn game_over_event(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    texture_atlases: ResMut<Assets<TextureAtlas>>,
    segments: ResMut<SnakeSegments>,
    mut game_over_reader: EventReader<GameOverEvent>,
    mut spawn_food_writer: EventWriter<SpawnFoodEvent>,
    food: Query<Entity, With<Food>>,
    snake: Query<Entity, With<SnakeSegment>>,
) {
    if game_over_reader.iter().next().is_some() {
        for entity in food.iter().chain(snake.iter()) {
            commands.entity(entity).despawn();
        }
        spawn_snake(commands, asset_server, texture_atlases, segments);
        spawn_food_writer.send(SpawnFoodEvent);
    }
}

fn sprites(
    segment_entity_query: ResMut<SnakeSegments>,
    mut sprite_query: Query<&mut TextureAtlasSprite, With<SnakeSegment>>,
    position_query: Query<&Position>,
) {
    let segment_len = segment_entity_query.0.len();

    // head
    let head_entity = segment_entity_query.0[0];
    let head_prev_pos = position_query.get(segment_entity_query.0[1]).unwrap();
    let head_pos = position_query.get(head_entity).unwrap();

    let mut head_sprite = sprite_query.get_mut(head_entity).unwrap();
    head_sprite.index = match head_pos.direction_to(*head_prev_pos) {
        Direction::Up => SnakeSpriteIndex::HeadDown,
        Direction::Left => SnakeSpriteIndex::HeadRight,
        Direction::Down => SnakeSpriteIndex::HeadUp,
        Direction::Right => SnakeSpriteIndex::HeadLeft,
    } as usize;

    // segments between head and tail
    for win in segment_entity_query.0.windows(3) {
        let next_pos = position_query.get(win[0]).unwrap();
        let pos = position_query.get(win[1]).unwrap();
        let prev_pos = position_query.get(win[2]).unwrap();

        let mut sprite = sprite_query.get_mut(win[1]).unwrap();
        sprite.index = match (pos.direction_to(*next_pos), pos.direction_to(*prev_pos)) {
            (Direction::Up, Direction::Left) | (Direction::Left, Direction::Up) => {
                SnakeSpriteIndex::TopLeft
            }
            (Direction::Up, Direction::Down) | (Direction::Down, Direction::Up) => {
                SnakeSpriteIndex::Vertical
            }
            (Direction::Up, Direction::Right) | (Direction::Right, Direction::Up) => {
                SnakeSpriteIndex::TopRight
            }
            (Direction::Left, Direction::Down) | (Direction::Down, Direction::Left) => {
                SnakeSpriteIndex::BottomLeft
            }
            (Direction::Left, Direction::Right) | (Direction::Right, Direction::Left) => {
                SnakeSpriteIndex::Horizontal
            }
            (Direction::Down, Direction::Right) | (Direction::Right, Direction::Down) => {
                SnakeSpriteIndex::BottomRight
            }
            _ => unreachable!(),
        } as usize;
    }

    // tail
    let tail_entity = segment_entity_query.0[segment_len - 1];
    let tail_next_pos = position_query
        .get(segment_entity_query.0[segment_len - 2])
        .unwrap();
    let tail_pos = position_query.get(tail_entity).unwrap();

    let mut tail_sprite = sprite_query.get_mut(tail_entity).unwrap();
    tail_sprite.index = match tail_pos.direction_to(*tail_next_pos) {
        Direction::Up => SnakeSpriteIndex::TailUp,
        Direction::Left => SnakeSpriteIndex::TailLeft,
        Direction::Down => SnakeSpriteIndex::TailDown,
        Direction::Right => SnakeSpriteIndex::TailRight,
    } as usize;
}
